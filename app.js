const customDesign = angular.module('topohatch', []);

customDesign.config(['$compileProvider', function ($compileProvider) {
    customDesign.compileProvider = $compileProvider;
}]);

customDesign.constant('CONFIG', {
    'APP_NAME' : 'Topohatch',
    'APP_VERSION' : '2.0',
    'GOOGLE_ANALYTICS_ID' : 'UA-99190927-1',
    'WEB_VERSION' : window.location.pathname.indexOf('rework') !== -1 ? 'custom' : 'material'
});

customDesign.controller('mainController', ['$scope', '$location', '$sce', '$window', function ($scope, $location, $sce, $window) {
    $scope.siteVersion = ($location.absUrl().indexOf('rework') !== -1) ? 'pages/material/' : 'pages/custom/';
    $scope.gmapsUrl = 'https://www.google.com/maps/embed?pb=!1m21!1m12!1m3!1d1366.0533370334813!2d23.61227188365443!3d46.78250234739235!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m6!3e0!4m0!4m3!3m2!1d46.7824829!2d23.6130133!5e0!3m2!1sen!2sro!4v1494053062100';
    $scope.directiveArray = angular.module('topohatch')._invokeQueue;
    $scope.directiveMap = {
        welcome: true,
        aboutUs: true,
        aboutCompany: true,
        services: false,
        prices: false,
        info: false,
        contact: false
    };
    $scope.activeNavItem = function (page) {
        return page === $location.hash() ? "active" : "";
    };

    $scope.trustSrc = function (src) {
        return $sce.trustAsResourceUrl(src);
    };

    /*lazy load directives*/
/*    $scope.lazyLoad = function () {
        for (var i = 0; i < $scope.directiveArray.length; i++) {
            for (var c = 0; c < Object.keys($scope.directiveMap).length; c++) {
                if ((!$scope.directiveMap[$scope.directiveArray[i][2][0]]) && ($scope.directiveArray[i][2][0] === Object.keys($scope.directiveMap)[c])) {
                    $scope.directiveMap[$scope.directiveArray[i][2][0]] = true;
                    return;
                }
            }
        }
    };*/

    /*Lazy load event*/
/*    angular.element($window).bind("scroll", function () {
        var windowHeight = "innerHeight" in window ? window.innerHeight : document.documentElement.offsetHeight,
            body = document.body, html = document.documentElement,
            docHeight = Math.max(body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, html.offsetHeight),
            windowBottom = windowHeight + window.pageYOffset;
        if (windowBottom >= docHeight) {
            $scope.$apply($scope.lazyLoad());
        }
    });*/
}]);


/**
 * Views
 */
customDesign.directive("header", ['CONFIG', function (CONFIG) {
    return {
        restrict: 'AE',
        templateUrl: function(){
            return `pages/${CONFIG.WEB_VERSION}/header.html`;
        },
        replace: true
    }
}]);

customDesign.directive("welcome", ['CONFIG', function (CONFIG) {
    return {
        restrict: 'E',
        templateUrl: `pages/${CONFIG.WEB_VERSION}/welcome.html`,
        replace: true
    }
}]);

customDesign.directive("aboutUs", ['CONFIG', function (CONFIG) {
    return {
        restrict: 'E',
        templateUrl: `pages/${CONFIG.WEB_VERSION}/about-us.html`,
        replace: true
    }
}]);

customDesign.directive("aboutCompany", ['CONFIG', function (CONFIG) {
    return {
        restrict: 'E',
        templateUrl: `pages/${CONFIG.WEB_VERSION}/about-company.html`,
        replace: true
    }
}]);

customDesign.directive("services", ['CONFIG', function (CONFIG) {
    return {
        restrict: 'E',
        templateUrl: `pages/${CONFIG.WEB_VERSION}/services.html`,
        replace: true
    }
}]);

customDesign.directive("prices", ['CONFIG', function (CONFIG) {
    return {
        restrict: 'E',
        templateUrl: `pages/${CONFIG.WEB_VERSION}/prices.html`,
        replace: true
    }
}]);


customDesign.directive("info", ['CONFIG', function (CONFIG) {
    return {
        restrict: 'E',
        templateUrl: `pages/${CONFIG.WEB_VERSION}/info.html`,
        replace: true
    }
}]);

customDesign.directive("contact", ['CONFIG', function (CONFIG) {
    return {
        restrict: 'E',
        templateUrl: `pages/${CONFIG.WEB_VERSION}/contact.html`,
        replace: true
    }
}]);

customDesign.directive("callNow", ['CONFIG', function (CONFIG) {
    return {
        restrict: 'E',
        templateUrl: `pages/${CONFIG.WEB_VERSION}/call-now.html`,
        replace: true
    }
}]);

customDesign.directive("linksSection", ['CONFIG', function (CONFIG) {
    return {
        restrict: 'E',
        templateUrl: `pages/${CONFIG.WEB_VERSION}/links-section.html`,
        replace: true
    }
}]);

customDesign.directive("footer", ['CONFIG', function (CONFIG) {
    return {
        restrict: 'E',
        templateUrl: `pages/${CONFIG.WEB_VERSION}/footer.html`,
        replace: true
    }
}]);

/**
 * Dom manipulation
 */
customDesign.directive('hideParentOnClick', function () {
    return {
        link: function (scope, element) {
            element.on('click', function () {
                if(element.find('.modal-view').length){
                    element.find('.modal-view').toggle();
                } else if(element.find('.info-body').length){
                    element.find('.info-body').toggle();
                }
            });
        }
    }
});
